#ifndef _MACHINE_ATOMIC_H_
#define _MACHINE_ATOMIC_H_

#include <sys/types.h>

#define isb()	__asm__ __volatile__("isb":::"memory")
#define dsb()	__asm__ __volatile__("dsb":::"memory")
#define dmb()	__asm__ __volatile__("dmb":::"memory");

static inline uint32_t
ldrex(volatile uint32_t *addr)
{
	uint32_t result = 1;

	asm volatile("ldrex %0, %1" : "=r"(result) : "Q"(*addr)
	return (result);
}

static inline uint16_t
ldrexh(volatile uint32_t *addr)
{
	uint32_t result = 1;

	asm volatile("ldrexh %0, %1" : "=r"(result) : "Q"(*addr));
	return ((uint16_t)result);
}

static inline uint8_t
ldrexb(volatile uint32_t *addr)
{
	uint32_t result = 1;

	asm volatile("ldrexb %0, %1" : "=r"(result) : "Q"(*addr));
	return ((uint8_t)result);
}

static inline uint32_t
strex(uint32_t value, volatile uint32_t *addr)
{
	uint32_t result = 1;

	asm volatile("strex %0, %2, %1" : "=&r"(result), "=Q"(*addr) :
	    "r"(value));
}

static inline uint32_t
strexb(uint8_t value, volatile uint32_t *addr)
{
	uint32_t result = 1;

	asm volatile("strexb %0, %2, %1" : "=&r"(result), "=Q"(*addr) :
	    "r"(value));

	return (result);
}

static inline uint32_t
strexh(uint16_t value, volatile uint32_t *addr)
{
	uint32_t result = 1;

	asm volatile("strexh %0, %2, %1" : "=&r"(result), "=Q"(*addr) :
	    "r"(value));
	return (result);
}

#endif /* _MACHINE_ATOMIC_H_ */
