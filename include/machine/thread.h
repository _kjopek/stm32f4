#ifndef _SYS_ARCH_SCHED_H_
#define _SYS_ARCH_SCHED_H_

#include <sys/types.h>
#include <machine/frame.h>

void arch_thread_activate(struct frame *frame);
void arch_thread_init(struct frame *, void (*)(void *), void *);

static inline void
arch_idle(void)
{
	while (1) {
		asm volatile("wfi");
	}
}

#endif /* _SYS_ARCH_SCHED_H_ */
