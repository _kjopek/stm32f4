#ifndef _SYS_MACHINE_FRAME_H_
#define _SYS_MACHINE_FRAME_H_

#include <sys/assert.h>
#include <sys/types.h>

struct frame {
	int32_t		r4;
	int32_t		r5;
	int32_t		r6;
	int32_t		r7;
	int32_t		r8;
	int32_t		r9;
	int32_t		r10;
	int32_t		r11;
	uint32_t	exc_lr;
	int32_t		r0;
	int32_t		r1;
	int32_t		r2;
	int32_t		r3;
	int32_t 	r12; /* also: IP */
	uint32_t	lr;
	uint32_t	pc;
	uint32_t	xpsr;
#ifdef __HARDFP__
	uint32_t	s0;
	uint32_t	s1;
	uint32_t	s2;
	uint32_t	s3;
	uint32_t	s4;
	uint32_t	s5;
	uint32_t	s6;
	uint32_t	s7;
	uint32_t	s8;
	uint32_t	s9;
	uint32_t	s10;
	uint32_t	s11;
	uint32_t	s12;
	uint32_t	s13;
	uint32_t	s14;
	uint32_t	s15;
	uint32_t	fpscr;
#endif
};

CTASSERT(sizeof(uint32_t) == sizeof(int32_t), "Integer types size mismatch.");
#ifndef __HARDFP__
CTASSERT(sizeof(struct frame) == 17 * sizeof(uint32_t), "Invalid frame size.");
#else
CTASSERT(sizeof(struct frame) == 34 * sizeof(uint32_t), "Invalid frame size.");
#endif

struct thread_arch {
	uint32_t	ontrol;
};

CTASSERT(sizeof(struct thread_arch) == sizeof(uint32_t),
    "Invalid size of arch-specific thread structure.");

#endif /* SYS_MACHINE_FRAME_H_ */
