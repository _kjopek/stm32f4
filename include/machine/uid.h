#ifndef _UID_H_
#define _UID_H_

#include <sys/types.h>

struct uid {
	uint16_t	x;
	uint16_t	y;
	uint8_t		wafer;
	uint8_t		lot[7];
};

#define UID	((struct uid *) 0x1FFF7A10LU)
#define UID_SIZE	(sizeof(struct uid))


#endif /* _UID_H_ */
