#ifndef _SYS_MACHINE_ABORT_H_
#define _SYS_MACHINE_ABORT_H_

void arch_abort(void) __attribute__((noreturn));

#endif /* _SYS_MACHINE_ABORT_H_ */
