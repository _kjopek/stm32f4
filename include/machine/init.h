#ifndef _MACHINE_INIT_H_
#define _MACHINE_INIT_H_

void reset(void) __attribute__((noreturn));
void initialize_gpio(void);

#endif /* _MACHINE_INIT_H_ */
