#ifndef _SYS_SCHED_H_
#define _SYS_SCHED_H_

#define THREADS_MAX	16

struct frame;

void sched_thread_idle(void *);
struct frame *sched_tick(struct frame *thread_stack);
void sched_init(void);

#endif /* _SYS_SCHED_H_ */
