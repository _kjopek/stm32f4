#ifndef _SYS_INIT_H
#define _SYS_INIT_H

void sys_init(void *arg) __attribute__((noreturn));
void user_init(void *arg);

#endif /* _SYS_INIT_H */
