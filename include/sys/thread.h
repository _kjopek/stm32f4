#ifndef _SYS_THREAD_H_
#define _SYS_THREAD_H_

#include <machine/frame.h>

#define THREAD_STACK_SIZE	256
#define THREAD_QUANT_DEFAULT	20

enum thread_state {
	THREAD_STATE_FREE = 0,
	THREAD_STATE_RUNNING,
	THREAD_STATE_WAITING,
	THREAD_STATE_SLEEPING,
	THREAD_STATE_KILLED
};

enum thread_privilege {
	THREAD_PRIV_SYSTEM = 0,
	THREAD_PRIV_USER,
	THREAD_PRIV_DRIVER
};

struct thread_stack {
	uint32_t	 base[THREAD_STACK_SIZE];
};

struct thread_quant {
	uint32_t	 value;
	uint32_t	 sleep;
};

struct thread {
	struct thread_arch	 arch;	/* arch-specific values. */
	struct thread_quant	 quant;
	struct frame		*frame;
	enum thread_state	 state;
	struct thread_stack	 stack;
};

#endif /* _SYS_THREAD_H_ */
