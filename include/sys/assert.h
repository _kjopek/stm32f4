#ifndef _SYS_ASSERT_H_
#define _SYS_ASSERT_H_

#define ASSERT(cond) ((cond) ? (void)0 : abort())
#define CTASSERT(cond, msg) _Static_assert((cond), msg)

void abort(void);

#endif /* _SYS_ASSERT_H_ */
