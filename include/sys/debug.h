#ifndef _SYS_DEBUG_H_
#define _SYS_DEBUG_H_

#include <sys/types.h>

void debug_init(void);
void debug_send(const char *buffer, uint32_t len);

#endif /* _SYS_DEBUG_H_ */
