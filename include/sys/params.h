#ifndef PARAMS_H
#define PARAMS_H

#define STM32F40_41xxx

#include <sys/types.h>
#include <stm32f4xx.h>

#define STACK_BASE	(SRAM2_BASE + STACK_SIZE)
#define STACK_SIZE	0x00004000U

/* Linker symbols. */
/* Data */
extern const uint32_t linker_etext;
extern uint32_t linker_data_start;
extern const uint32_t linker_data_end;

/* BSS */
extern uint32_t linker_bss_start;
extern const uint32_t linker_bss_end;

#endif
