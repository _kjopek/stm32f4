#ifndef _SYS_API_H_
#define _SYS_API_H_

#include <sys/types.h>
#include <sys/event.h>
#include <sys/thread.h>

struct system_version {
	const uint8_t		 major;
	const uint8_t		 minor;
	const uint8_t		 patch;
	const uint8_t		 addon;
};

struct thread_api {
	void	(*const yield)(void);
	void	(*const exit)(int32_t);
};

struct event_api {
	int	(*const queue)(struct event *, int *);
};

struct message_api {
	int	(*const send)(const void *buffer, uint32_t len);
	int	(*const recv)(void *buffer, uint32_t len);
};

struct system_api {
	const struct system_version	version;
	const struct thread_api		thread;
	const struct event_api		event;
	const struct message_api	message;
};

extern const struct system_api api;

#endif /* _SYS_API_H_ */
