#ifndef _SYS_TYPES_H_
#define _SYS_TYPES_H_

#include <sys/assert.h>

#define NULL (void *)0

typedef unsigned char uint8_t;
typedef char int8_t;

typedef unsigned short uint16_t;
typedef short int16_t;

typedef unsigned int uint32_t;
typedef int int32_t;

typedef unsigned long long uint64_t;
typedef long long int64_t;

#ifndef CTASSERT
CTASSERT(sizeof(uint8_t) == 1, "Invalid uint8_t size.");
CTASSERT(sizeof(int8_t) == 1, "Invalid int8_t size.");
CTASSERT(sizeof(uint16_t) == 2, "Invalid uint16_t size.");
CTASSERT(sizeof(int16_t) == 2, "Invalid int16_t size.");
CTASSERT(sizeof(uint32_t) == 4, "Invalid uint32_t size.");
CTASSERT(sizeof(int32_t) == 4, "Invalid int32_t size.");
CTASSERT(sizeof(uint64_t) == 8, "Invalid uint64_t size.");
CTASSERT(sizeof(int64_t) == 8, "Invalid int64_t size.");
#endif /* CTASSERT */

#endif /* _SYS_TYPES_H_ */
