SRCS = src/abort.c \
       src/init.c \
       src/sched.c \
       src/kern.c \
       src/api.c \
       src/user.c \
       src/arch/abort.c \
       src/arch/init.c \
       src/arch/thread.c \
       src/arch/debug.c \
       src/arch/rng.c

INCS += include/stdint.h \
	include/sys/params.h \
	include/sys/init.h \
	include/sys/event.h \
	include/sys/types.h \
	include/sys/assert.h \
	include/sys/sched.h \
	include/sys/api.h \
	include/sys/debug.h \
	include/sys/thread.h \
	include/stm32f4xx_conf.h \
	include/machine/frame.h \
	include/machine/abort.h \
	include/machine/arch.h \
	include/machine/uid.h \
	include/machine/thread.h


#CC=arm-none-eabi-gcc
#LD=arm-none-eabi-ld

MK_MAN=no
MCFLAGS=-target armv7m-none-eabi -mcpu=cortex-m4 -mlittle-endian \
	-mfpu=fpv4-sp-d16 -mfloat-abi=hard

LD=ld.lld
SSP_CFLAGS=
DEFS= -DUSE_STDPERIPH_DRIVER -DSTM32F4XX -D__SOFTFP__
CFLAGS= $(DEFS) $(MCFLAGS) -fno-builtin -nostdlib -nostdinc -O3 -ggdb -Iinclude \
	-Ivendor/STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Device/ST/STM32F4xx/Include \
	-Ivendor/STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries/CMSIS/Include/ \
	-Wall -Werror
# LINKER FLAGS
LDSCRIPT= stm32f4_flash.ld
LDFLAGS= -T$(LDSCRIPT) -nostdlib -fno-stack-protector

PROG= test

.include <bsd.prog.mk>
