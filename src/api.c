#include <sys/api.h>

/* Yielding time. */
void __attribute__((naked))
libc_yield(void)
{
	asm volatile("svc #0");
	asm volatile("bx lr");
}

/* Thread manipulation. */
void __attribute__((naked))
libc_exit(int32_t retcode)
{
	asm volatile("svc #1");
	asm volatile("bx lr");
}

int __attribute__((naked))
libc_fork(void (*func)(void *arg), void *arg)
{
	asm volatile("svc #2");
	asm volatile("bx lr");
}

/* Event collector */
int __attribute__((naked))
libc_event(struct event *events, int *events_len)
{
	asm volatile("svc #3");
	asm volatile("bx lr");
}

const struct system_api api = {
	.version = {
		.major = 0U,
		.minor = 1U,
		.patch = 0U,
		.addon = 0U,
	},
	.thread = {
		.yield = libc_yield,
		.exit = libc_exit,
	},
	.event = {
		.queue = libc_event,
	},
	.message = {
		.send = NULL,
		.recv = NULL,
	}
};
