#include <sys/types.h>
#include <sys/api.h>
#include <machine/frame.h>

struct syscall {
	int32_t	(*call)();
	int32_t	nargs;
};

int32_t
sys_yield(void)
{

	return (0);
}

int32_t
sys_exit(uint32_t return_code)
{

	return (0);
}

int32_t
sys_fork(void (*func)(void *), void *arg)
{

	return (0);
}

int32_t
sys_event(struct event *events, int *events_len)
{

	return (0);
}

static const struct syscall syscalls[] = {
	{
		.call = sys_yield,
		.nargs = 0
	},
	{
		.call = sys_exit,
		.nargs = 1
	},
	{
		.call = sys_fork,
		.nargs = 2
	},
	{
		.call = sys_event,
		.nargs = 2
	}
};

void *
sys_enter(struct frame *frame)
{
	int svc = (int)*((unsigned char *)(frame->pc - 2));
	if (svc < 0 || svc >= sizeof(syscalls) / sizeof(struct syscall))
		return (frame);

	syscalls[svc].call(frame->r0, frame->r1, frame->r2, frame->r3);
	return (frame);
}
