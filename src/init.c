#include <sys/types.h>
#include <sys/params.h>
#include <sys/sched.h>

#include <sys/api.h>

#include <machine/init.h>
#include <machine/rng.h>

void
sys_init(void *arg)
{
	(void)arg;
	uint32_t ii;

	initialize_gpio();
	rng_init();
	for (ii = 0;;ii = (ii + 1) % 4) {
		GPIOD->ODR = (1 << (ii + 12));
		__WFI();
	}
}
