#include <machine/abort.h>

void
arch_abort(void)
{
	for (;;) {
		asm volatile ("bkpt ":::);
	}
	/* NOTREACHED */
}
