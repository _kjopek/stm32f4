#include <sys/assert.h>
#include <sys/types.h>
#include <sys/params.h>
#include <sys/sched.h>

#include <sys/debug.h>

#define PLL_M	25
#define PLL_Q	7
#define PLL_N	336
#define PLL_P	2

typedef void (*func_handler_t)(void);

void
rcc_init(void)
{
	__IO uint32_t hse_status;
	__IO uint32_t cr, cfgr;

	RCC->CR |= 0x00000001U;
	RCC->CFGR = 0x00000000U;
	RCC->CR &= 0xFEF6FFFFU;

	RCC->PLLCFGR = 0x24003010U;
	RCC->CR &= 0xFFFBFFFFU;
	RCC->CIR = 0x00000000U;
	SCB->VTOR = FLASH_BASE;

	RCC->CR |= (uint32_t)RCC_CR_HSEON;

	do {
		hse_status = RCC->CR & RCC_CR_HSERDY;
	} while (hse_status == 0);

	if ((RCC->CR & RCC_CR_HSERDY) == RESET)
		abort();

	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	PWR->CR |= PWR_CR_VOS;

	RCC->CFGR |= RCC_CFGR_HPRE_DIV1;

	/* PCLK2 = HCLK / 2*/
	RCC->CFGR |= RCC_CFGR_PPRE2_DIV2;

	/* PCLK1 = HCLK / 4*/
	RCC->CFGR |= RCC_CFGR_PPRE1_DIV4;

	/* Configure the main PLL */
	RCC->PLLCFGR = PLL_M | (PLL_N << 6) | (((PLL_P >> 1) - 1) << 16) |
	    (RCC_PLLCFGR_PLLSRC_HSE) | (PLL_Q << 24);

	/* Enable the main PLL */
	RCC->CR |= RCC_CR_PLLON;

	/* Wait till the main PLL is ready */
	do {
		cr = RCC->CR & RCC_CR_PLLRDY;
	} while (cr == 0);

	/* Configure Flash prefetch, Instruction cache, Data cache and wait state */
	FLASH->ACR = FLASH_ACR_PRFTEN | FLASH_ACR_ICEN | FLASH_ACR_DCEN |
	    FLASH_ACR_LATENCY_5WS;

	/* Select the main PLL as system clock source */
	RCC->CFGR &= (uint32_t)~RCC_CFGR_SW;
	RCC->CFGR |= RCC_CFGR_SW_PLL;

	/* Wait till the main PLL is used as system clock source */
	do {
		cfgr = RCC->CFGR & (uint32_t)RCC_CFGR_SWS;
	} while (cfgr != RCC_CFGR_SWS_PLL);
}

void
systick_init(void)
{
	/*
	 * Clock is 168000000.
	 *
	 * For ten ms we need therefore 168000000 HZ / 16800000 = 10 Hz
	 */
	SysTick->LOAD = 1680000 - 1;
	SysTick->VAL = SysTick->LOAD;
	NVIC_SetPriority(SysTick_IRQn, (1 << __NVIC_PRIO_BITS) - 1);
	SysTick->CTRL = SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_ENABLE_Msk;
}

const char *message = "TEST";

void
reset_handler(void)
{
	uint32_t *ptr;
	uint32_t *flash_ptr;

	flash_ptr = (uint32_t *)&linker_etext;
	/* Copy global data. */
	for (ptr = &linker_data_start; ptr < &linker_data_end; ptr++)
		*ptr = *flash_ptr++;

	/* Zeroing bss. */
	for (ptr = &linker_bss_start; ptr < &linker_bss_end; ptr++)
		*ptr = 0U;

	rcc_init();
	debug_init();
	debug_send(message, sizeof(message));
	systick_init();
	sched_init();
}

void initialize_gpio(void)
{
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
	GPIOD->MODER |= GPIO_MODER_MODER12_0 | GPIO_MODER_MODER13_0 | GPIO_MODER_MODER14_0 | GPIO_MODER_MODER15_0;
	GPIOD->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR12 | GPIO_OSPEEDER_OSPEEDR13 | GPIO_OSPEEDER_OSPEEDR14 | GPIO_OSPEEDER_OSPEEDR15;
	GPIOD->PUPDR = 0;
	GPIOD->ODR |= GPIO_ODR_ODR_15 | GPIO_ODR_ODR_14;
}

static void
nmi_handler(void)
{

	for (;;) {}
}

static void
hardfault_handler(void)
{

	for (;;) {}
}

static void
memmanage_handler(void)
{

	for (;;) {}
}

static void
busfault_handler(void)
{

	for (;;) {}
}

static void
usagefault_handler(void)
{

	for (;;) {}
}

static void __attribute__((naked))
svccall_handler(void)
{
	asm volatile("mrs r0, psp");
	asm volatile("stmdb r0!, {r4-r11, lr}");
	asm volatile("bl sys_enter");
	asm volatile("ldmia r0!, {r4-r11, lr}");
	asm volatile("msr psp, r0");
	asm volatile("bx lr");
}

static void
debug_monitor(void)
{

	for (;;) {}
}

static void
pendsv_handler(void)
{

	for (;;) {}
}

void __attribute__((naked))
systick_handler(void)
{
	asm volatile("mrs r0, psp");
	asm volatile("stmdb r0!, {r4-r11, lr}");
	asm volatile("bl sched_tick");
	asm volatile("ldmia r0!, {r4-r11, lr}");
	asm volatile("msr psp, r0");
	asm volatile("isb");
	asm volatile("bx lr");
}

const func_handler_t isr_vectors[0x61] __attribute__((section(".isr_vector"))) = {
	(func_handler_t)STACK_BASE,	/* 0 */
	reset_handler,			/* 1 */
	nmi_handler,			/* 2 */
	hardfault_handler,		/* 3 */
	memmanage_handler,		/* 4 */
	busfault_handler,		/* 5 */
	usagefault_handler,		/* 6 */
	/* reserved */
	[7 ... 10] = reset_handler,	/* 7 - 10 */
	svccall_handler,		/* 11 */
	debug_monitor,			/* 12 */
	/* reserved */
	reset_handler,			/* 13 */
	pendsv_handler,			/* 14 */
	systick_handler,		/* 15 */
	[16 ... 96] = reset_handler	/* 16 - 96 */
};
