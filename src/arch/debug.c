#include <sys/types.h>
#include <sys/params.h>
#include <sys/debug.h>


void
debug_init(void)
{
	CoreDebug->DEMCR = CoreDebug_DEMCR_TRCENA_Msk;
	CoreDebug->DEMCR = CoreDebug_DEMCR_VC_CORERESET_Msk;
	ITM->LAR = 0xC5ACCE55UL;
	ITM->TCR |= ITM_TCR_DWTENA_Msk;
	ITM->TCR |= ITM_TCR_SYNCENA_Msk;
	ITM->TER = 0x01UL;
	ITM->TPR = 0x01;
	ITM->TCR = ITM_TCR_ITMENA_Msk;
}

void
debug_send(const char *buffer, uint32_t len)
{
	for (; len > 0; len--)
		ITM_SendChar(*buffer++);
}
