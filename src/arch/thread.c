#include <sys/types.h>
#include <sys/thread.h>

void arch_thread_enter(void *stack);

void __attribute__((naked, noreturn))
arch_thread_activate(struct frame *frame)
{
	asm volatile("ldmia r0!, {r4-r11, lr}");
	asm volatile("msr psp, r0");
	asm volatile("isb");
	asm volatile("mov r0, #2");
	asm volatile("msr control, r0");
	asm volatile("ldr pc, [sp, 24]");
}

void
arch_thread_init(struct frame *frame, void (*entry)(void *), void *arg)
{

	frame->r4 = 0L;
	frame->r5 = 0L;
	frame->r6 = 0L;
	frame->r7 = 0L;
	frame->r8 = 0L;
	frame->r9 = 0L;
	frame->r10 = 0L;
	frame->r11 = 0L;
	frame->exc_lr = 0xFFFFFFFDUL;
	frame->r0 = (uint32_t)arg;
	frame->r1 = 0L;
	frame->r2 = 0L;
	frame->r3 = 0L;
	frame->r12 = 0L;
	frame->lr = 0UL;
	frame->pc = (uint32_t)entry;
	frame->xpsr = 1UL << 24;
}
