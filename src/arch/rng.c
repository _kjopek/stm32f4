#include <sys/types.h>
#include <sys/params.h>


void
rng_init(void)
{
	RCC->AHB2ENR |= RCC_AHB2ENR_RNGEN;
	RNG->CR |= RNG_CR_IE;
	RNG->CR	|= RNG_CR_RNGEN;
}
