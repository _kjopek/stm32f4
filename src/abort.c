#include <sys/assert.h>
#include <machine/abort.h>

void
abort()
{
	arch_abort();
	for (;;) {}
	/* UNREACHABLE */
}
