#include <sys/types.h>
#include <sys/thread.h>
#include <sys/sched.h>
#include <sys/init.h>
#include <machine/frame.h>
#include <machine/thread.h>

static struct thread threads[THREADS_MAX];
static int curr_thread = 0;

static int32_t
thread_init(void (*entry)(void *), void *arg)
{
	int32_t id = 0;

	while (id < THREADS_MAX && threads[id].state != THREAD_STATE_FREE)
		id++;

	if (id == THREADS_MAX)
		return (-1L);

	threads[id].frame = (struct frame *)((uint32_t *)&threads[id].stack +
	    THREAD_STACK_SIZE - sizeof(struct frame) / sizeof(uint32_t));
	threads[id].state = THREAD_STATE_RUNNING;
	threads[id].quant.value = THREAD_QUANT_DEFAULT;
	threads[id].quant.sleep = 0;

	arch_thread_init(threads[id].frame, entry, arg);
	return (id);
}

void
sched_init(void)
{

	/*
	 * Init idle thread.
	 *
	 * In our case this is unprivileged thread that runs
	 * as thread 0.
	 */
	thread_init(sched_thread_idle, NULL);

	/*
	 * Main system thread.
	 *
	 * Also - kernel init subroutine.
	 */
	thread_init(sys_init, NULL);

	/*
	 * User thread.
	 */
	thread_init(user_init, NULL);

	arch_thread_activate(threads[0].frame);
}

struct frame *
sched_tick(struct frame *frame)
{
	int ii, thr_idx;

	threads[curr_thread].frame = frame;
	if (threads[curr_thread].quant.value > 1) {
		threads[curr_thread].quant.value--;
		return (threads[curr_thread].frame);
	}

	for (ii = 0; ii < THREADS_MAX - 1; ii++) {
		thr_idx = (curr_thread + ii) % (THREADS_MAX - 1) + 1;

		if (threads[thr_idx].state == THREAD_STATE_RUNNING) {
			curr_thread = thr_idx;
			threads[curr_thread].quant.value = THREAD_QUANT_DEFAULT;
			return (threads[curr_thread].frame);
		}
	}

	curr_thread = 0;
	return (threads[curr_thread].frame);
}

void
sched_thread_idle(void *arg)
{
	(void)arg;

	arch_idle();
}
